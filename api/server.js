import app from './app.js'

const PORT = 1000

app.get("/", (req, res) => {
    res.send(`Server Berjalan di Port ${PORT}`)
})

app.listen(PORT, () =>{
    console.log(`Server Running On Port ${PORT}`)
})